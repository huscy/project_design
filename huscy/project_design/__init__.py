# major.minor.patch.release.number
# release must be one of alpha, beta, rc or final
VERSION = (0, 5, 0, 'beta', 20)

__version__ = '.'.join(str(x) for x in VERSION)
