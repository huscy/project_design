from django.contrib import admin

from huscy.project_design import models


admin.site.register(models.Experiment)
admin.site.register(models.Session)
