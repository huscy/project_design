from itertools import cycle

import pytest
from model_bakery import baker

from huscy.project_design.services import get_sessions

pytestmark = pytest.mark.django_db


@pytest.fixture
def sessions():
    experiments = baker.make('project_design.Experiment', _quantity=2)
    return baker.make('project_design.Session', experiment=cycle(experiments), _quantity=6)


def test_get_sessions(sessions):
    experiment = sessions[1].experiment

    result = get_sessions(experiment)

    assert len(result) == 3
    assert list(result) == sorted([sessions[1], sessions[3], sessions[5]], key=lambda x: x.order)
