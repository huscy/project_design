from itertools import cycle

import pytest
from model_bakery import baker

from huscy.project_design.services import get_data_acquisition_methods

pytestmark = pytest.mark.django_db


@pytest.fixture
def data_acquisition_methods():
    sessions = baker.make('project_design.Session', _quantity=2)
    return baker.make('project_design.DataAcquisitionMethod', session=cycle(sessions), _quantity=4)


def test_get_data_acquisition_methods(data_acquisition_methods):
    session = data_acquisition_methods[0].session

    result = get_data_acquisition_methods(session)

    assert len(result) == 2
    assert list(result) == sorted([data_acquisition_methods[0], data_acquisition_methods[2]],
                                  key=lambda x: x.order)
