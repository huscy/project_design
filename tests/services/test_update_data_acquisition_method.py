from datetime import timedelta

import pytest

from huscy.project_design.models import DataAcquisitionMethod
from huscy.project_design.services import update_data_acquisition_method

pytestmark = pytest.mark.django_db


def test_update_data_acquisition_method(questionaire):
    duration = timedelta(minutes=60)
    location = 'C201'
    setup_time = timedelta(minutes=30)
    stimulus = DataAcquisitionMethod.STIMULUS.visual
    teardown_time = timedelta(minutes=15)

    result = update_data_acquisition_method(questionaire, duration=duration, location=location,
                                            setup_time=setup_time, teardown_time=teardown_time,
                                            stimulus=stimulus)

    assert result.duration == duration
    assert result.setup_time == setup_time
    assert result.teardown_time == teardown_time
    assert result.location == location
    assert result.stimulus == stimulus

    questionaire.refresh_from_db()
    assert questionaire == result


def test_update_duration(questionaire):
    duration = timedelta(minutes=60)

    result = update_data_acquisition_method(questionaire, duration=duration)

    assert result.duration == duration
    questionaire.refresh_from_db()
    assert questionaire == result


def test_update_location(questionaire):
    location = 'C201'

    result = update_data_acquisition_method(questionaire, location=location)

    assert result.location == location

    questionaire.refresh_from_db()
    assert questionaire == result


def test_update_setup_time(questionaire):
    setup_time = timedelta(minutes=30)

    result = update_data_acquisition_method(questionaire, setup_time=setup_time)

    assert result.setup_time == setup_time
    questionaire.refresh_from_db()
    assert questionaire == result


def test_update_stimulus(questionaire):
    stimulus = DataAcquisitionMethod.STIMULUS.visual

    result = update_data_acquisition_method(questionaire, stimulus=stimulus)

    assert result.stimulus == stimulus

    questionaire.refresh_from_db()
    assert questionaire == result


def test_update_teardown_time(questionaire):
    teardown_time = timedelta(minutes=15)

    result = update_data_acquisition_method(questionaire, teardown_time=teardown_time)

    assert result.teardown_time == teardown_time
    questionaire.refresh_from_db()
    assert questionaire == result


def test_update_nothing(questionaire):
    result = update_data_acquisition_method(questionaire)

    assert result == questionaire


def test_raise_error_if_field_cannot_be_updated(questionaire):
    with pytest.raises(ValueError) as error:
        update_data_acquisition_method(questionaire, order=5)

    assert str(error.value) == 'Cannot update field "order".'
