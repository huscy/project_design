from itertools import cycle

import pytest
from model_bakery import baker

from huscy.project_design.services import get_experiments

pytestmark = pytest.mark.django_db


@pytest.fixture
def experiments():
    projects = baker.make('projects.Project', _quantity=2)
    return baker.make('project_design.Experiment', project=cycle(projects), _quantity=4)


def test_get_experiments(experiments):
    project = experiments[1].project

    result = get_experiments(project)

    assert len(result) == 2
    assert list(result) == sorted([experiments[1], experiments[3]], key=lambda x: x.order)
