from datetime import timedelta

import pytest
from model_bakery import baker

from huscy.project_design.services import create_session

pytestmark = pytest.mark.django_db


def test_create_session(experiment):
    baker.make('project_design.Session', _quantity=2)  # two random sessions

    session = create_session(experiment)

    assert session.contacts.count() == 0
    assert session.data_acquisition_methods.count() == 0
    assert session.experiment == experiment
    assert session.order == 0
    assert session.title == 'Session 1'


def test_create_session_when_experiment_has_already_two_sessions(experiment):
    baker.make('project_design.Session', _quantity=2)  # two random sessions
    baker.make('project_design.Session', experiment=experiment, _quantity=2)

    session = create_session(experiment)

    assert session.experiment == experiment
    assert session.order == 2
    assert session.title == 'Session 3'


def test_create_session_with_title(experiment):
    session = create_session(experiment, title='new session')

    assert session.experiment == experiment
    assert session.order == 0
    assert session.title == 'new session'


def test_create_session_with_data_acquisition_methods(experiment):
    baker.make('project_design.DataAcquisitionMethodType', short_name='mri')
    baker.make('project_design.DataAcquisitionMethodType', short_name='questionaire')

    session = create_session(
        experiment,
        data_acquisition_methods=[
            {'type': 'mri', 'duration': timedelta(hours=1)},
            {'type': 'questionaire', 'duration': timedelta(hours=1)}
        ],
    )

    assert session.data_acquisition_methods.count() == 2
    assert session.duration == timedelta(hours=2)
    assert session.experiment == experiment


def test_create_session_with_contacts(django_user_model, experiment):
    users = baker.make(django_user_model, _quantity=2)

    session = create_session(experiment, contacts=users)

    assert session.contacts.count() == 2
    assert session.experiment == experiment
