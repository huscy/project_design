import pytest

from huscy.project_design.models import DataAcquisitionMethodType
from huscy.project_design.services import get_data_acquisition_method_type

pytestmark = pytest.mark.django_db


def test_get_data_acquisition_method_type(mri):
    result = get_data_acquisition_method_type('mri')

    assert result.pk == 'mri'
    assert result.short_name == 'mri'


def test_data_acquisition_method_type_does_not_exist():
    with pytest.raises(DataAcquisitionMethodType.DoesNotExist):
        get_data_acquisition_method_type('none')
