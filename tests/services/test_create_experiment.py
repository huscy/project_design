from datetime import timedelta

import pytest
from model_bakery import baker

from huscy.project_design.services import create_experiment

pytestmark = pytest.mark.django_db


def test_create_experiment(project, data_acquisition_method_type):
    baker.make('project_design.Experiment', _quantity=2)

    experiment = create_experiment(project, sessions=[
        {
            'data_acquisition_methods': [
                {
                    'duration': timedelta(minutes=60),
                    'type': data_acquisition_method_type,
                },
            ],
        },
    ])

    assert experiment.description == ''
    assert experiment.order == 0
    assert experiment.project == project
    assert experiment.sessions.count() == 1
    assert experiment.title == 'Experiment 1'

    session = experiment.sessions.first()
    assert session.data_acquisition_methods.count() == 1
    assert session.order == 0
    assert session.title == 'Session 1'

    data_acquisition_method = session.data_acquisition_methods.first()
    assert data_acquisition_method.duration == timedelta(seconds=3600)
    assert data_acquisition_method.order == 0


def test_create_experiment_without_sessions(project):
    baker.make('project_design.Experiment', _quantity=2)

    experiment = create_experiment(project)

    assert experiment.description == ''
    assert experiment.order == 0
    assert experiment.project == project
    assert experiment.sessions.count() == 0
    assert experiment.title == 'Experiment 1'


def test_create_experiment_when_project_has_already_two_experiments(project):
    baker.make('project_design.Experiment', project=project, _quantity=2)

    experiment = create_experiment(project)

    assert experiment.description == ''
    assert experiment.order == 2
    assert experiment.project == project
    assert experiment.title == 'Experiment 3'


def test_create_experiment_with_title(project):
    experiment = create_experiment(project, title='the experiment')

    assert experiment.description == ''
    assert experiment.order == 0
    assert experiment.project == project
    assert experiment.title == 'the experiment'
