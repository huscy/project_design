import pytest

from huscy.project_design.services import update_experiment

pytestmark = pytest.mark.django_db


def test_update_experiment(experiment):
    result = update_experiment(experiment, title='new title', description='new description')

    assert result.title == 'new title'
    assert result.description == 'new description'
    experiment.refresh_from_db()
    assert experiment == result


def test_update_experiment_description(experiment):
    result = update_experiment(experiment, description='new description')

    assert result.description == 'new description'
    experiment.refresh_from_db()
    assert experiment == result


def test_update_experiment_title(experiment):
    result = update_experiment(experiment, title='new title')

    assert result.title == 'new title'
    experiment.refresh_from_db()
    assert experiment == result


def test_update_nothing(experiment):
    result = update_experiment(experiment)

    assert result == experiment


def test_raise_error_if_field_cannot_be_updated(experiment):
    with pytest.raises(ValueError) as error:
        update_experiment(experiment, project=1)

    assert str(error.value) == 'Cannot update field "project".'
