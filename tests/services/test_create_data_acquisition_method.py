from datetime import timedelta

import pytest

from huscy.project_design.services import create_data_acquisition_method

pytestmark = pytest.mark.django_db


@pytest.fixture
def duration():
    return timedelta(hours=1)


def test_create_data_acquisition_method(session, data_acquisition_method_type, duration):
    result = create_data_acquisition_method(session, data_acquisition_method_type, duration,
                                            'location')

    assert result.duration == duration
    assert result.session == session
    assert result.type == data_acquisition_method_type
    assert result.location == 'location'


def test_create_with_method_type_as_string(session, data_acquisition_method_type, duration):
    result = create_data_acquisition_method(session, data_acquisition_method_type.short_name,
                                            duration, 'location')

    assert result.duration == duration
    assert result.session == session
    assert result.type == data_acquisition_method_type
    assert result.location == 'location'


def test_raise_value_error_when_type_has_unknown_data_type(session):
    with pytest.raises(ValueError) as error:
        create_data_acquisition_method(session, ['mri'], 'location')

    assert str(error.value) == 'Unknown data type for `type` attribute'
