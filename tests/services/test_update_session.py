import pytest
from model_bakery import baker

from huscy.project_design.services import update_session

pytestmark = pytest.mark.django_db


def test_update_contacts(django_user_model, session):
    users = baker.make(django_user_model, _quantity=3)

    assert session.contacts.count() == 0

    result = update_session(session, contacts=users)

    assert result.contacts.count() == 3
    session.refresh_from_db()
    assert session == result


def test_update_title(session):
    result = update_session(session, title='new title')

    assert result.title == 'new title'
    session.refresh_from_db()
    assert session == result


def test_raise_error_if_field_cannot_be_updated(session):
    with pytest.raises(ValueError) as error:
        update_session(session, order=5)

    assert str(error.value) == 'Cannot update field "order".'
