from datetime import timedelta

import pytest
from model_bakery import baker

from rest_framework.test import APIClient


ONE_HOUR = timedelta(hours=1)


@pytest.fixture
def user(django_user_model):
    return django_user_model.objects.create_user(username='user', password='password',
                                                 first_name='Gerd', last_name='Nehrei')


@pytest.fixture
def admin_client(admin_user):
    client = APIClient()
    client.login(username=admin_user.username, password='password')
    return client


@pytest.fixture
def client(user):
    client = APIClient()
    client.login(username=user.username, password='password')
    return client


@pytest.fixture
def anonymous_client():
    return APIClient()


@pytest.fixture
def project():
    return baker.make('projects.Project')


@pytest.fixture
def experiment(project):
    return baker.make('project_design.Experiment', project=project, description='description')


@pytest.fixture
def session(experiment):
    return baker.make('project_design.Session', experiment=experiment)


@pytest.fixture
def data_acquisition_method_type():
    return baker.make('project_design.DataAcquisitionMethodType')


@pytest.fixture
def mri(session):
    type = baker.make('project_design.DataAcquisitionMethodType', short_name='mri')
    return baker.make('project_design.DataAcquisitionMethod', session=session, type=type,
                      duration=ONE_HOUR)


@pytest.fixture
def questionaire(session):
    type = baker.make('project_design.DataAcquisitionMethodType', short_name='questionaire')
    return baker.make('project_design.DataAcquisitionMethod', session=session, type=type,
                      duration=ONE_HOUR)
