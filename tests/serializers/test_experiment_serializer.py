from itertools import cycle

import pytest
from model_bakery import baker

from huscy.project_design.serializer import ExperimentSerializer, SessionSerializer

pytestmark = pytest.mark.django_db


def test_sessions_are_serialized_in_experiment_serializer(experiment):
    sessions = baker.make('project_design.Session', experiment=experiment, order=cycle([0, 1]),
                          _quantity=2)

    serialized_experiment = ExperimentSerializer(experiment).data

    assert serialized_experiment['sessions'] == SessionSerializer(sessions, many=True).data
