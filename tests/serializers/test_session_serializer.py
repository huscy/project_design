import pytest
from model_bakery import baker

from huscy.project_design.serializer import (DataAcquisitionMethodSerializer, SessionSerializer,
                                             UserSerializer)

pytestmark = pytest.mark.django_db


def test_session_serializer(session, mri):
    data = SessionSerializer(session).data

    assert data['contacts'] == []
    assert data['duration'] == '01:00:00'
    assert data['data_acquisition_methods'] == DataAcquisitionMethodSerializer([mri],
                                                                               many=True).data


def test_contacts(django_user_model, session):
    contact = baker.make(django_user_model)
    session.contacts.add(contact)

    data = SessionSerializer(session).data

    assert data['contacts'] == UserSerializer([contact], many=True).data


def test_duration(session, mri, questionaire):
    data = SessionSerializer(session).data

    assert data['duration'] == '02:00:00'
    assert data['data_acquisition_methods'] == DataAcquisitionMethodSerializer([mri, questionaire],
                                                                               many=True).data
