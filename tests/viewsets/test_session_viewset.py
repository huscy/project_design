from datetime import timedelta

import pytest
from model_bakery import baker

from rest_framework.reverse import reverse
from rest_framework.status import (HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT,
                                   HTTP_403_FORBIDDEN, HTTP_405_METHOD_NOT_ALLOWED)

from django.contrib.auth.models import Permission

pytestmark = pytest.mark.django_db

ONE_HOUR = timedelta(hours=1)
TWO_HOURS = timedelta(hours=2)


# TODO: write test for creating a session without any data acquisition method


@pytest.fixture
def contacts(django_user_model):
    return baker.make(django_user_model, _quantity=2)


def test_admin_user_can_create_sessions(admin_client, experiment, mri, contacts):
    response = create_session(admin_client, experiment, contacts)

    assert response.status_code == HTTP_201_CREATED


def test_admin_user_can_delete_sessions(admin_client, session):
    response = delete_session(admin_client, session)

    assert response.status_code == HTTP_204_NO_CONTENT


def test_list_sessions_is_not_supported(admin_client, experiment):
    response = list_sessions(admin_client, experiment)

    assert response.status_code == HTTP_405_METHOD_NOT_ALLOWED


def test_admin_user_can_partial_update_sessions(admin_client, session):
    response = partial_update_session(admin_client, session)

    assert response.status_code == HTTP_200_OK


def test_admin_user_can_update_sessions(admin_client, session, contacts):
    response = update_session(admin_client, session, contacts)

    assert response.status_code == HTTP_200_OK


def test_user_with_permission_can_create_sessions(client, user, experiment, mri, contacts):
    create_permission = Permission.objects.get(codename='add_session',
                                               content_type__app_label='project_design')
    user.user_permissions.add(create_permission)

    response = create_session(client, experiment, contacts)

    assert response.status_code == HTTP_201_CREATED


def test_user_with_permission_can_delete_sessions(client, user, session):
    delete_permission = Permission.objects.get(codename='delete_session',
                                               content_type__app_label='project_design')
    user.user_permissions.add(delete_permission)

    response = delete_session(client, session)

    assert response.status_code == HTTP_204_NO_CONTENT


def test_user_with_permission_can_partial_update_sessions(client, user, session):
    update_permission = Permission.objects.get(codename='change_session',
                                               content_type__app_label='project_design')
    user.user_permissions.add(update_permission)

    response = partial_update_session(client, session)

    assert response.status_code == HTTP_200_OK


def test_user_with_permission_can_update_sessions(client, user, session, contacts):
    update_permission = Permission.objects.get(codename='change_session',
                                               content_type__app_label='project_design')
    user.user_permissions.add(update_permission)

    response = update_session(client, session, contacts)

    assert response.status_code == HTTP_200_OK


def test_user_without_permission_can_create_sessions(client, experiment, mri, contacts):
    response = create_session(client, experiment, contacts)

    assert response.status_code == HTTP_201_CREATED


def test_user_without_permission_can_delete_sessions(client, session):
    response = delete_session(client, session)

    assert response.status_code == HTTP_204_NO_CONTENT


def test_user_without_permission_can_partial_update_sessions(client, session):
    response = partial_update_session(client, session)

    assert response.status_code == HTTP_200_OK


def test_user_without_permission_can_update_sessions(client, session, contacts):
    response = update_session(client, session, contacts)

    assert response.status_code == HTTP_200_OK


def test_anonymous_user_cannot_create_sessions(anonymous_client, experiment, contacts):
    response = create_session(anonymous_client, experiment, contacts)

    assert response.status_code == HTTP_403_FORBIDDEN


def test_anonymous_user_cannot_delete_sessions(anonymous_client, session):
    response = delete_session(anonymous_client, session)

    assert response.status_code == HTTP_403_FORBIDDEN


def test_anonymous_user_cannot_partial_update_sessions(anonymous_client, session):
    response = partial_update_session(anonymous_client, session)

    assert response.status_code == HTTP_403_FORBIDDEN


def test_anonymous_user_cannot_update_sessions(anonymous_client, session, contacts):
    response = update_session(anonymous_client, session, contacts)

    assert response.status_code == HTTP_403_FORBIDDEN


def create_session(client, experiment, contacts):
    project = experiment.project
    return client.post(
        reverse('session-list', kwargs=dict(project_pk=project.pk, experiment_pk=experiment.pk)),
        data=dict(
            contacts=[contact.id for contact in contacts],
            data_acquisition_methods=[{'type': 'mri', 'duration': timedelta(hours=1)}],
            experiment=experiment.pk,
        )
    )


def delete_session(client, session):
    experiment = session.experiment
    project = experiment.project
    return client.delete(reverse('session-detail', kwargs=dict(project_pk=project.pk,
                                                               experiment_pk=experiment.pk,
                                                               pk=session.pk)))


def list_sessions(client, experiment):
    project = experiment.project
    return client.get(reverse('session-list', kwargs=dict(project_pk=project.pk,
                                                          experiment_pk=experiment.pk)))


def partial_update_session(client, session):
    experiment = session.experiment
    project = experiment.project
    return client.patch(
        reverse('session-detail', kwargs=dict(project_pk=project.pk, experiment_pk=experiment.pk,
                                              pk=session.pk)),
        data=dict(title='new title')
    )


def update_session(client, session, contacts):
    experiment = session.experiment
    project = experiment.project
    return client.put(
        reverse('session-detail', kwargs=dict(project_pk=project.pk, experiment_pk=experiment.pk,
                                              pk=session.id)),
        data=dict(
            contacts=[contact.id for contact in contacts],
            title='new title',
        )
    )
