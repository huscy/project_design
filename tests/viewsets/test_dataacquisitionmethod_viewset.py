from datetime import timedelta

import pytest
from model_bakery import baker

from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_403_FORBIDDEN

from huscy.project_design.models import DataAcquisitionMethod

pytestmark = pytest.mark.django_db


def test_admin_user_can_create_data_acquisition_methods(admin_client, session):
    response = create_data_acquisition_method(admin_client, session)

    assert response.status_code == HTTP_201_CREATED


def test_admin_user_can_update_data_acquisition_method(admin_client, questionaire):
    response = update_data_acquisition_method(admin_client, questionaire)

    assert response.status_code == HTTP_200_OK


def test_user_without_permission_can_create_data_acquisition_methods(client, session):
    response = create_data_acquisition_method(client, session)

    assert response.status_code == HTTP_201_CREATED


def test_anonymous_user_cannot_create_data_acquisition_methods(anonymous_client, session):
    response = create_data_acquisition_method(anonymous_client, session)

    assert response.status_code == HTTP_403_FORBIDDEN


def create_data_acquisition_method(client, session):
    data_acquisition_method_type = baker.make('project_design.DataAcquisitionMethodType')
    experiment = session.experiment
    project = experiment.project
    return client.post(
        reverse('dataacquisitionmethod-list', kwargs=dict(project_pk=project.pk,
                                                          experiment_pk=experiment.pk,
                                                          session_pk=session.pk)),
        data=dict(type=data_acquisition_method_type.pk, duration=timedelta(hours=1))
    )


def update_data_acquisition_method(client, data_acquisition_method):
    session = data_acquisition_method.session
    experiment = session.experiment
    project = experiment.project
    return client.put(
        reverse('dataacquisitionmethod-detail', kwargs=dict(project_pk=project.pk,
                                                            experiment_pk=experiment.pk,
                                                            session_pk=session.pk,
                                                            pk=data_acquisition_method.pk)),
        data=dict(
            duration=data_acquisition_method.duration,
            location='C206',
            setup_time=data_acquisition_method.setup_time,
            stimulus=DataAcquisitionMethod.STIMULUS.visual,
            teardown_time=data_acquisition_method.teardown_time,
        )
    )
